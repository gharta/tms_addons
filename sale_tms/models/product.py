from odoo import models, fields, _, api
from odoo.exceptions import ValidationError
from odoo.addons import decimal_precision as dp

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    transportation_price = fields.Float(
        'Transportation Price', default=1.0,
        digits=dp.get_precision('Product Price'))
    fuel_price = fields.Float(
        'Fuel Price', default=1.0,
        digits=dp.get_precision('Product Price'))
    wage_driver = fields.Float(
        'Wage', default=1.0,
        digits=dp.get_precision('Product Price'))
    
    route_id = fields.Many2one('tms.route', 'Route')
    route_lead_time = fields.Float('Route Lead Time')
    
    @api.onchange('route_id')
    def onchange_route_id(self):
    	if self.route_id.id:
    		self.type = 'service'

    @api.constrains('type')
    def check_type_id(self):
    	if self.route_id.id != False and self.type != 'service':
    		raise ValidationError(_('Product type of route must be service'))
  
    @api.onchange('transportation_price', 'fuel_price', 'wage_driver')
    def _onchange_wage_price(self):
#         self.list_price = self.transportation_price + self.fuel_price + self.wage_driver
        self.standard_price = self.transportation_price + self.fuel_price + self.wage_driver
        
    
class Travel(models.Model):
    _inherit = 'tms.travel'
    
    route_lead_time = fields.Float('Lead Time', compute="_get_lead_time", store=True)
    
    @api.depends('route_id')
    def _get_lead_time(self):
        for rec in self:
            if not rec.route_id.id:
                continue
            product = self.env['product.template'].search([('route_id', '=', rec.route_id.id)], limit=1)
            rec.route_lead_time = product.route_lead_time
    
        