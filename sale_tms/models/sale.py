from odoo import models, fields, api

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        for line in self.order_line:
            if line.route_id.id:
                travel_vals = line._prepare_travel()
                travel_id = self.env['tms.travel'].create(travel_vals)
                line.travel_id = travel_id.id
        return res
    
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    route_id = fields.Many2one('tms.route', 'Route', related='product_id.route_id')
    unit_id = fields.Many2one('fleet.vehicle', 'Unit')
    driver_id = fields.Many2one('hr.employee', 'Driver')
    travel_id = fields.Many2one('tms.travel', "Travel")
    cost_price = fields.Float('Cost', related="product_id.standard_price", store=True, readonly=True)
    
    @api.multi
    def _prepare_travel(self):
        return {
            'operating_unit_id':self.operating_unit_id.id,
            'route_id':self.route_id.id,
            'employee_id':self.driver_id.id,
            'unit_id':self.unit_id.id,
            'so_id':self.order_id.id
            }
    