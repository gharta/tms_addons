# -*- coding: utf-8 -*-
from odoo import http
import functools
import logging
import sys
import json
import time
import hashlib
import os
import logging
from ast import literal_eval
try:
    import simplejson as json
except ImportError:
    import json

import werkzeug.utils
from werkzeug.exceptions import BadRequest
import werkzeug.wrappers

import odoo
from odoo import api, http, SUPERUSER_ID, _
from odoo.http import request
from odoo import registry as registry_get
from odoo.exceptions import AccessDenied
from odoo.addons.auth_signup.models.res_users import SignupError
import requests
from datetime import datetime, timedelta
import re


class TMSAPI(http.Controller):

    def get_formatted(self, currency, value) :
        if currency.symbol and currency.position :
            if currency.position == 'after' :
                return str(value)+' '+currency.symbol
            else :
                return currency.symbol+' '+str(value)
        return str(value)

    def url_main_image(self, product_id):
        return request.env['ir.config_parameter'].sudo().get_param('web.base.url')+'/web/image?model=product.template&id='+str(product_id)+'&field=image'

    def url_multi_image(self, image_id):
        return request.env['ir.config_parameter'].sudo().get_param('web.base.url')+'/web/image?model=product.image&id='+str(image_id)+'&field=image'

    def validate_params(self, dictionary, params):
        res = True
        for param in params :
            if param not in dictionary :
                res = False
                return res
        return res

    # New API with REST
    def error_response(self, error_code, error, error_message):
        return werkzeug.wrappers.Response(
            status = error_code,
            content_type = 'application/json; charset=utf-8',
            #headers = None,
            response = json.dumps({
                'error_code': error_code,
                'error': error,
                'error_message': error_message,
            }),
        )

    def check_session_id(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            try :
                if not request.session.uid :
                    return self.error_response(401, 'Unauthorized', 'Unauthorized')
                else :
                    request.uid = request.session.uid
            except :
                return self.error_response(500, 'Error', 'Failed Login')
            
            # The code, following the decorator
            return func(self, *args, **kwargs)
        return wrapper

    @http.route('/api/login', type="http", auth="none", methods=['POST'], csrf=False, cors='*')
    def login(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        res = {}
        User = request.env['res.users'].sudo()
        Partner = request.env['res.partner'].sudo()

        if jdata and self.validate_params(jdata, ['login', 'password']) :
            login = jdata.get('login', False)
            password = jdata.get('password', False)
            db = request.db
            
            try :
                uid = request.session.authenticate(db, login, password)
                if uid :
                    request.uid = uid
                    user = User.search([('id','=',uid)])
                    partner = user.partner_id

                    res = { 
                        'id' : uid,
                        'name' : partner.name,
                        'username' : login,
                        'session_id' : request.env['ir.http'].session_info()['session_id'],
                        }
                else :
                    return self.error_response(500, 'Login Failed', 'Login or Password Invalid.') 
            except :
                return self.error_response(500, 'Login Failed', 'Login Failed.') 
        else :
            return self.error_response(500, 'Invalid Params', 'Invalid Parameters Sent.')  

        return json.dumps(res)

    @http.route('/api/travels', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def get_travels(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        Employee = request.env['hr.employee'].sudo()
        employee = Employee.search([('user_id', '=', request.uid)])

        if not employee or not employee.driver:
            return self.error_response(500, 'Error', 'You are not registered as driver.') 

        Travel = request.env['tms.travel'].sudo()
        travels = Travel.search([('employee_id','=',employee.id)])

        res = []
        for tr in travels :
            vals = {
                'id'   : tr.id,
                'name'   : tr.name,
                'unit_id' : {'id':tr.unit_id.id,'name':tr.unit_id.name} if tr.unit_id else {},
                'departure_id' : {'id':tr.departure_id.id,'name':tr.departure_id.name,'latitude':tr.departure_id.latitude,'longitude':tr.departure_id.longitude} if tr.departure_id else {},
                'arrival_id' : {'id':tr.arrival_id.id,'name':tr.arrival_id.name,'latitude':tr.arrival_id.latitude,'longitude':tr.arrival_id.longitude} if tr.arrival_id else {},
                'employee_id' : {'id':tr.employee_id.id,'name':tr.employee_id.name} if tr.employee_id else {},
                'date' : tr.date,
                'user_id' : {'id':tr.user_id.id,'name':tr.user_id.name} if tr.user_id else {},
                'state' : tr.state,
                'waybill_ids' : []
            }
            for wb in tr.waybill_ids :
                vals['waybill_ids'].append({
                    'id' : wb.id,
                    'name' : wb.name,
                    'partner_id' : {'id':wb.partner_id.id,'name':wb.partner_id.name}
                })

            res.append(vals)
        
        return json.dumps(res)

    @http.route('/api/travels/<int:travel_id>', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def get_travel(self, travel_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}


        Employee = request.env['hr.employee'].sudo()
        employee = Employee.search([('user_id', '=', request.uid)])

        if not employee or not employee.driver:
            return self.error_response(500, 'Error', 'You are not registered as driver.') 

        Travel = request.env['tms.travel'].sudo()
        if travel_id :
            try :
                tr = Travel.browse(travel_id)
            except :
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Cannot get Travel records.' + str(e)) 
        else :
            return self.error_response(500, 'Error', 'Travel Id not set.') 

        res = {}
        if tr and tr.employee_id.id == employee.id :
            res = {
                'id'   : tr.id,
                'name'   : tr.name,
                'unit_id' : {'id':tr.unit_id.id,'name':tr.unit_id.name} if tr.unit_id else {},
                'departure_id' : {'id':tr.departure_id.id,'name':tr.departure_id.name,'latitude':tr.departure_id.latitude,'longitude':tr.departure_id.longitude} if tr.departure_id else {},
                'arrival_id' : {'id':tr.arrival_id.id,'name':tr.arrival_id.name,'latitude':tr.arrival_id.latitude,'longitude':tr.arrival_id.longitude} if tr.arrival_id else {},
                'employee_id' : {'id':tr.employee_id.id,'name':tr.employee_id.name} if tr.employee_id else {},
                'date' : tr.date,
                'user_id' : {'id':tr.user_id.id,'name':tr.user_id.name} if tr.user_id else {},
                'state' : tr.state,
                'waybill_ids' : []
            }
            for wb in tr.waybill_ids :
                res['waybill_ids'].append({
                    'id' : wb.id,
                    'name' : wb.name,
                    'partner_id' : {'id':wb.partner_id.id,'name':wb.partner_id.name}
                })
        else :
            return self.error_response(500, 'Error', 'Travel with Id not found or you dont have permission to access') 
        
        return json.dumps(res)

    @http.route('/api/travels/<int:travel_id>/dispatch', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def dispatch_travel(self, travel_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        Employee = request.env['hr.employee'].sudo()
        employee = Employee.search([('user_id', '=', request.uid)])

        if not employee or not employee.driver:
            return self.error_response(500, 'Error', 'You are not registered as driver.') 

        Travel = request.env['tms.travel'].sudo()
        if travel_id :
            try :
                tr = Travel.browse(travel_id)
            except :
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Cannot get Travel records.' + str(e)) 
        else :
            return self.error_response(500, 'Error', 'Travel Id not set.') 

        res = {}
        if tr and tr.employee_id.id == employee.id :
            if tr.state!='draft' :
                return self.error_response(500, 'Error', 'Travel cannot be dispatched because of its state is not draft.')
            try:
                tr.action_progress()
                # Save fuel value
                if "fuel" in jdata and jdata['fuel'] :
                    tr.fuel_start = jdata['fuel']

                # Save odometer value
                headers = {
                    'Authorization': 'Basic YWRtaW46YWRtaW4=',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                }
                traccar_url_config = request.env['ir.config_parameter'].sudo().search([('key','=','traccar_url')])
                traccar_device_id = tr.unit_id.traccar_device_id
                url = traccar_url_config.value
                if url and traccar_device_id :
                    url = url + "/api/positions"
                    r = requests.get(url, headers=headers)
                    if r.status_code == requests.codes.ok :
                        response = r.json()
                        for r in response :
                            if traccar_device_id == r['deviceId'] :
                                if 'odometer' in r['attributes'] :
                                    tr.odometer_start = r['attributes']['odometer']

            except:
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Travel cannot be dispatched.' + str(e)) 
  
        else :
            return self.error_response(500, 'Error', 'Travel with Id not found or you dont have permission to access') 
        
        return json.dumps(res)


    @http.route('/api/travels/<int:travel_id>/cancel', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def cancel_travel(self, travel_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        Employee = request.env['hr.employee'].sudo()
        employee = Employee.search([('user_id', '=', request.uid)])

        if not employee or not employee.driver:
            return self.error_response(500, 'Error', 'You are not registered as driver.') 

        Travel = request.env['tms.travel'].sudo()
        if travel_id :
            try :
                tr = Travel.browse(travel_id)
            except :
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Cannot get Travel records.' + str(e)) 
        else :
            return self.error_response(500, 'Error', 'Travel Id not set.') 

        res = {}
        if tr and tr.employee_id.id == employee.id :
            try:
                tr.action_cancel()
            except:
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Travel cannot be dispatched.' + str(e)) 
  
        else :
            return self.error_response(500, 'Error', 'Travel with Id not found or you dont have permission to access') 
        
        return json.dumps(res)


    @http.route('/api/travels/<int:travel_id>/finish', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def finish_travel(self, travel_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        Employee = request.env['hr.employee'].sudo()
        employee = Employee.search([('user_id', '=', request.uid)])

        if not employee or not employee.driver:
            return self.error_response(500, 'Error', 'You are not registered as driver.') 

        Travel = request.env['tms.travel'].sudo()
        if travel_id :
            try :
                tr = Travel.browse(travel_id)
            except :
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Cannot get Travel records.' + str(e)) 
        else :
            return self.error_response(500, 'Error', 'Travel Id not set.') 

        res = {}
        if tr and tr.employee_id.id == employee.id :
            if tr.state!='progress' :
                return self.error_response(500, 'Error', 'Travel cannot be finished because it has not been dispatched.')
            try:
                tr.action_done()
                # Save fuel value
                if "fuel" in jdata and jdata['fuel'] :
                    tr.fuel_end = jdata['fuel']
                # Save odometer value
                headers = {
                    'Authorization': 'Basic YWRtaW46YWRtaW4=',
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                }
                traccar_url_config = request.env['ir.config_parameter'].sudo().search([('key','=','traccar_url')])
                traccar_device_id = tr.unit_id.traccar_device_id
                url = traccar_url_config.value
                if url and traccar_device_id :
                    url = url + "/api/positions"
                    r = requests.get(url, headers=headers)
                    if r.status_code == requests.codes.ok :
                        response = r.json()
                        for r in response :
                            if traccar_device_id == r['deviceId'] :
                                if 'odometer' in r['attributes'] :
                                    tr.odometer_end = r['attributes']['odometer']
            except:
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Travel cannot be dispatched.' + str(e)) 
  
        else :
            return self.error_response(500, 'Error', 'Travel with Id not found or you dont have permission to access') 
        
        return json.dumps(res)


    @http.route('/api/messages', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def get_messages(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        User = request.env['res.users'].sudo()
        user = User.browse(request.uid)
        Partner = request.env['res.partner'].sudo()
        user_admin = request.env['ir.model.data'].sudo().get_object('base', 'user_root')
        if not user_admin :
            return self.error_response(500, 'Error', 'User Admin not found') 
        partner_admin = user_admin.partner_id

        Channel = request.env['mail.channel'].sudo()
        private_channel = Channel.search([('name', 'like', user_admin.name),('name', 'like', user.name)])
        if private_channel :
            private_channel = private_channel[0]
        general_channel = Channel.search([('name', 'like', 'general')])
        if general_channel :
            general_channel = general_channel[0]

        Message = request.env['mail.message'].sudo()
        channel = private_channel or general_channel
        if channel :
            messages = Message.search([('message_type','=','comment'), ('channel_ids','in',channel.id)])
        else :
            messages = Message.search([('message_type','=','comment')])

        res = []
        for msg in messages :
            vals = {
                'id'   : msg.id,
                'subject' : msg.subject,
                'body' : re.sub("<[^>]*>", "", msg.body),
                'author_id' : {'id':msg.author_id.id,'name':msg.author_id.name},
                'create_date' : msg.create_date,
                'message_type' : msg.message_type,
            }
            vals['partner_ids'] = []
            for partner in msg.partner_ids :
                vals['partner_ids'].append({
                    'id' : partner.id,
                    'name' : partner.name,
                })
            res.append(vals)
        
        return json.dumps(res)


    @http.route('/api/messages/create', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def create_message(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        User = request.env['res.users'].sudo()
        user = User.browse(request.uid)
        partner = user.partner_id
        Partner = request.env['res.partner'].sudo()
        Message = request.env['mail.message'].sudo()
        Channel = request.env['mail.channel'].sudo()

        user_admin = request.env['ir.model.data'].sudo().get_object('base', 'user_root')
        if not user_admin :
            return self.error_response(500, 'Error', 'User Admin not found') 
        partner_admin = user_admin.partner_id

        res = {}
        if not partner_admin :
            return self.error_response(500, 'Error', 'Partner not found.') 

        channel = Channel.search([('name', 'like', user_admin.name),('name', 'like', user.name)])
        if channel :
            channel = channel[0]
            channel.write({
                'channel_partner_ids' :  [(4, partner_admin.id), (4, partner.id)]
            })

        if not channel :
            vals = {
                'name' : user.name+','+user_admin.name,
                'channel_partner_ids' :  [(4, partner_admin.id), (4, partner.id)],
                'channel_type' : 'chat'
            }
            channel = Channel.create(vals)
        try :
            if 'body' in jdata: 
                channel.message_post(body=jdata['body'],content_subtype='html',message_type='comment',
                author_id=user.partner_id.id,partner_ids=[],subtype='mail.mt_comment')

            general_channel = Channel.search([('name', 'like', 'general')])
            if general_channel and 'body' in jdata:
                general_channel.message_post(body=jdata['body'],content_subtype='html',message_type='comment',
                author_id=user.partner_id.id,partner_ids=[],subtype='mail.mt_comment')

        except :
            return self.error_response(500, 'Error', 'Cannot post message.') 
        return json.dumps(res)

    @http.route('/api/callme', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def callme(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        res = {}
        User = request.env['res.users'].sudo()
        user = User.browse(request.uid)
        partner = user.partner_id
        Partner = request.env['res.partner'].sudo()
        Message = request.env['mail.message'].sudo()
        Channel = request.env['mail.channel'].sudo()

        user_admin = request.env['ir.model.data'].sudo().get_object('base', 'user_root')
        if not user_admin :
            return self.error_response(500, 'Error', 'User Admin to send callme request not found') 
        partner_admin = user_admin.partner_id

        channel = Channel.search([('name', 'like', user_admin.name),('name', 'like', user.name)])
        if channel :
            channel = channel[0]
            channel.write({
                'channel_partner_ids' :  [(4, partner_admin.id), (4, partner.id)]
            })

        if not channel :
            vals = {
                'name' : user.name+','+user_admin.name,
                'channel_partner_ids' :  [(4, partner_admin.id), (4, partner.id)],
                'channel_type' : 'chat'
            }
            channel = Channel.create(vals)
        try :
            msg = "Call me! " + (user.partner_id.phone or user.partner_id.mobile or "")
            channel.message_post(body=msg,content_subtype='html',message_type='comment',
                author_id=user.partner_id.id,partner_ids=[],subtype='mail.mt_comment')

        except :
            return self.error_response(500, 'Error', 'Cannot post message.') 
        return json.dumps(res)


    @http.route('/api/calllogs/<int:travel_id>', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def get_call_logs(self, travel_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        Employee = request.env['hr.employee'].sudo()
        employee = Employee.search([('user_id', '=', request.uid)])

        if not employee or not employee.driver:
            return self.error_response(500, 'Error', 'You are not registered as driver.') 

        Travel = request.env['tms.travel'].sudo()
        if travel_id :
            try :
                tr = Travel.browse(travel_id)
            except :
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Cannot get Travel records.' + str(e)) 
        else :
            return self.error_response(500, 'Error', 'Travel Id not set.') 

        res = []
        if tr and tr.employee_id.id == employee.id :
            for call_log in tr.call_log_ids :
                res.append({
                    'name' : call_log.name,
                    'call_time' : call_log.call_time,
                })
        else :
            return self.error_response(500, 'Error', 'Travel with Id not found or you dont have permission to access') 
        
        return json.dumps(res)


    @http.route('/api/calllogs/<int:travel_id>/create', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def create_call_log(self, travel_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        Employee = request.env['hr.employee'].sudo()
        employee = Employee.search([('user_id', '=', request.uid)])

        if not employee or not employee.driver:
            return self.error_response(500, 'Error', 'You are not registered as driver.') 

        Travel = request.env['tms.travel'].sudo()
        if travel_id :
            try :
                tr = Travel.browse(travel_id)
            except :
                e = sys.exc_info()
                return self.error_response(500, 'Error', 'Cannot get Travel records.' + str(e)) 
        else :
            return self.error_response(500, 'Error', 'Travel Id not set.') 

        res = {}
        if tr and tr.employee_id.id == employee.id :
            request.env['tms.call.log'].sudo().create({
                'name' : jdata.get('name') or 'Head Office',
                'call_time' : jdata.get('call_time') or datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'travel_id' : tr.id
            })
        else :
            return self.error_response(500, 'Error', 'Travel with Id not found or you dont have permission to access') 
        
        return json.dumps(res)


    @http.route('/api/fcmregid/set', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    @check_session_id
    def set_fcm_regid(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        User = request.env['res.users'].sudo()
        user = User.browse(request.uid)

        res = {}
        if user :
            if 'fcm_regid' in jdata and jdata['fcm_regid'] :
                user.fcm_regid = jdata['fcm_regid']

        return json.dumps(res)