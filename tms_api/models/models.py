# -*- coding: utf-8 -*-
import logging
from datetime import datetime

from odoo import api, fields, models, _
from odoo.addons.base_geoengine import fields as geo_fields
from odoo.addons.base_geoengine import geo_model
from odoo.exceptions import ValidationError, UserError
from odoo.http import request
import datetime

import simplejson as json

_logger = logging.getLogger(__name__)
try:
    from pyproj import Proj, transform
    import geojson
except ImportError:
    _logger.debug('Cannot `import pyproj or geojson`.')

import requests
import urllib2
import math

from dateutil import tz
from dateutil.relativedelta import relativedelta
from odoo.tools.float_utils import float_is_zero


# API_KEY = "AAAAH5c5Kq8:APA91bEfpp_Jz1uLm3MrSgtK8dcVPy_gsrCrmflXObG4c6fxScvyPoQfK-0Gy54zChOf6DPDS2Tb6yqgH82U4kdxUjfqPFaBPbEYyvvU9Ofk0ZcoOCo4_dMJP0-vvOXsbXKewQudoiEp"
# import time

class TravelCallLog(models.Model):
    _name = "tms.call.log"

    name = fields.Char('Name')
    call_time = fields.Datetime('Call Time')
    travel_id = fields.Many2one('tms.travel', 'Travel')

class User(models.Model):
    _inherit = "res.users"

    fcm_regid = fields.Char('FCM Regid')


class Travel(models.Model):
    _inherit = "tms.travel"

    call_log_ids = fields.One2many('tms.call.log', 'travel_id', 'Call Logs')

class TMSTravel(models.Model):
    _inherit = 'tms.travel'
    
    state = fields.Selection([
        ('draft', 'Pending'),
        ('loading', 'Loading'),
        ('progress', 'In Progress'),
        ('unloading', 'Unloading'),
        ('oot','Out of Track'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
        ('closed', 'Closed')
        ])
    
    fuel_start = fields.Float('Start Fuel')
    fuel_end = fields.Float('End Fuel')
    odometer_start = fields.Float('Start Odometer')
    odometer_end = fields.Float('End Odometer')
    
    loading_id = fields.Many2one("fleet.vehicle.log", "Loading In")
    loading_out_id = fields.Many2one("fleet.vehicle.log", "Loading Out")
    unloading_id = fields.Many2one("fleet.vehicle.log", "Unloading In")
    unloading_out_id = fields.Many2one("fleet.vehicle.log", "Unloading Out")
    distance_driver = fields.Float(compute='_compute_distance_gps')
    date_end_real = fields.Datetime(related="unloading_id.gps_time")
    date_start_real = fields.Datetime(related="loading_id.gps_time")
    travel_duration_real = fields.Float(compute="_compute_distance_gps")
    
    route_sequence = fields.Integer("Route Sequence", default=0)
    load_weight = fields.Float("Weight of loads (ton)")
    load_stay = fields.Float("Load Stay", compute="_get_load_stay", store=False)
    unload_stay = fields.Float("Unload Stay", compute="_get_unload_stay", store=False)
    loading_date = fields.Date("Loading Date",  compute="compute_loading_hour", store=True)
    unloading_date = fields.Date("UnLoading Date", compute="compute_unloading_hour", store=True)
    loading_hour = fields.Float("Loading Hour", compute="compute_loading_hour", store=True)
    unloading_hour = fields.Float("UnLoading Hour", compute="compute_unloading_hour", store=True)
    
    @api.depends('loading_id', 'loading_out_id')
    def _get_load_stay(self):
        for rec in self:
            if not rec.loading_id.gps_time or not rec.loading_out_id.gps_time:
                continue
            load_dt = datetime.datetime.strptime(rec.loading_id.gps_time,"%Y-%m-%d %H:%M:%S")
            load_out_dt = datetime.datetime.strptime(rec.loading_out_id.gps_time,"%Y-%m-%d %H:%M:%S")
            stay = load_out_dt - load_dt
            rec.load_stay = stay.seconds / 60.0000 / 60.0000
    
    @api.multi
    def _get_unload_stay(self):
        for rec in self:
            if not rec.unloading_id.gps_time or not rec.unloading_out_id.gps_time:
                continue
            unload_dt = datetime.datetime.strptime(rec.unloading_id.gps_time,"%Y-%m-%d %H:%M:%S")
            unload_out_dt = datetime.datetime.strptime(rec.unloading_out_id.gps_time,"%Y-%m-%d %H:%M:%S")
            stay = unload_out_dt - unload_dt
            rec.unload_stay = stay.seconds / 60.0000 / 60.0000
    
    @api.depends('loading_id')
    def compute_loading_hour(self):
        for rec in self:
            if rec.loading_id.id:
                load_dt = datetime.datetime.strptime(rec.loading_id.gps_time,"%Y-%m-%d %H:%M:%S")
                load_dtm = load_dt.minute / 60.0000
                load_dts = load_dt.second / 60.0000 / 60.0000
                rec.loading_hour = load_dt.hour + load_dtm + load_dts
                rec.loading_date = load_dt.date()
    
    @api.depends('unloading_id')
    def compute_unloading_hour(self):
        for rec in self:
            if rec.unloading_id.id:
                load_dt = datetime.datetime.strptime(rec.unloading_id.gps_time,"%Y-%m-%d %H:%M:%S")
                load_dtm = load_dt.minute / 60.0000
                load_dts = load_dt.second / 60.0000 / 60.0000
                rec.unloading_hour = load_dt.hour + load_dtm + load_dts
                rec.unloading_date = load_dt.date()
    
    @api.depends("loading_id", "unloading_id")
    def _compute_distance_gps(self):
        if self.unloading_id.id:
            loading_id =  self.loading_id
            unloading_id = self.unloading_id
            
            self.distance_driver = unloading_id.mileage - loading_id.mileage
            time_load = datetime.datetime.strptime(loading_id.gps_time,"%Y-%m-%d %H:%M:%S")
            time_unload = datetime.datetime.strptime(unloading_id.gps_time,"%Y-%m-%d %H:%M:%S")
            self.travel_duration_real = (time_unload - time_load).seconds/60.0/60.0
    
    @api.multi
    def check_distance(self, lat1, lon1, lat2, lon2):
        R = 6378.137; # Radius of earth in KM
        dLat = lat2 * math.pi / 180 - lat1 * math.pi / 180;
        dLon = lon2 * math.pi / 180 - lon1 * math.pi / 180;
        a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2);
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a));
        d = R * c;
        return d * 1000; # meters
    
    @api.multi
    def action_load(self):
        self.state = "loading"
    
    @api.multi
    def action_unload(self):
        self.state = "unloading"
    
    @api.multi
    def action_done(self):
        super(TMSTravel, self).action_done()
        for rec in self: 
            vehicle_logs = self.env['fleet.vehicle.log'].search([('vehicle_id','=',rec.unit_id.id),('active','=',True)])
            for log in vehicle_logs:
                log.active = False
            
    
    @api.multi
    def test_button(self):
        travel = self.env['tms.travel'].search([('id','=',self.id),('state','=','progress')], limit=1)
        if travel.id and travel.state != 'oot':
            target_pos = self.env['tms.route.log'].search([('route_id','=',travel.route_id.id)], order="id asc")
            cur_lat = travel.unit_id.log_line[0].latitude
            cur_lon = travel.unit_id.log_line[0].longitude
            count_target = len(target_pos) - 1
            while True:
                target_lat = target_pos[travel.route_sequence].latitude
                target_lon = target_pos[travel.route_sequence].longitude
                if travel.check_distance(cur_lat, cur_lon, target_lat, target_lon) > 500.0:
                    if travel.route_sequence < count_target:
                        travel.route_sequence += 1
                        continue 
                    else:
                        raise ValidationError("{} Out of Track".format(travel.unit_id.name))
                break
    
class TMSRoute(models.Model):
    _inherit = "tms.route"
    
    state = fields.Selection([('routing','Routing'),('ready','Ready')], "State", default="routing")
    log_ids = fields.One2many("tms.route.log", "route_id", "Logs")
    vehicle_id = fields.Many2one("fleet.vehicle", "Vehicle")
    
    @api.multi
    def act_ready(self):
        self.state = 'ready'
    
    @api.multi
    def act_route(self):
        self.state = 'routing'

    @api.multi
    def act_osrm_route(self):
        RouteLog = self.env['tms.route.log']
        for route in self :
            url_osrm_nearest = 'http://router.project-osrm.org/nearest/v1/driving/'
            url_osrm_route = 'http://router.project-osrm.org/route/v1/driving/'
            if route.departure_id and route.arrival_id :
                point1 = str(route.departure_id.longitude) + "," + str(route.departure_id.latitude)
                point2 = str(route.arrival_id.longitude) + "," + str(route.arrival_id.latitude)
                # oveview=simplified or oveview=full 
                url = url_osrm_route + point1 + ';' + point2 + '?steps=true&overview=simplified&annotations=true&geometries=geojson' 
                headers = {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                }
                r = requests.get(url, headers=headers)
                if r.status_code == requests.codes.ok :
                    response = r.json()
                    geom = response['routes'][0]['geometry']
                    coords = geom['coordinates']
                    route_logs = RouteLog.search([('route_id', '=', route.id)])
                    route_logs.unlink()
                    for idx, coord in enumerate(coords):
                        route_log = RouteLog.create({'longitude' : coord[0], 'latitude' : coord[1], 'route_id' : route.id})
                else :
                    response = r.json()
                    raise UserError(response['message'])

class TMSRouteLog(models.Model):
    _name = "tms.route.log"
    _description = "Route Logging"
    _rec_name = "gps_time"
    _order = "id desc"
    
    sim = fields.Char("SIM")
    longitude = fields.Float("Longitude", digits=(20,10))
    latitude = fields.Float("Latitude", digits=(20,10))
    high = fields.Float("High")
    speed = fields.Float("Speed")
    gps_time_str = fields.Char("GPS Time")
    gps_time = fields.Datetime("GPS Time")
    mileage = fields.Integer("Mileage")
    temperature = fields.Float("temperature")
    position = fields.Char("Position")
    vehicle_id = fields.Many2one("fleet.vehicle", "Vehicle")
    oil_num = fields.Float("Oil")
    route_id = fields.Many2one("tms.route","Route")
            
class FleetFence(geo_model.GeoModel):
    _name = 'fleet.fence'

    name = fields.Char('Name', required=True)
    area = geo_fields.GeoMultiPolygon(
        string='Area'
    )

class FleetVehicle(geo_model.GeoModel):
    _inherit = 'fleet.vehicle'

    latitude = fields.Float(
        required=False, digits=(20, 10),
        help='GPS Latitude')
    longitude = fields.Float(
        required=False, digits=(20, 10),
        help='GPS Longitude')
    point = geo_fields.GeoPoint(
        string='Coordinate',
        compute='_compute_point',
        inverse='_set_lat_long',
    )
    traccar_device_id = fields.Integer('Traccar Device ID')


    sim = fields.Text("SIM")
    # longitude = fields.Float("Longitude")
    # latitude = fields.Float("Latitude")
    high = fields.Float("High")
    speed = fields.Float("Speed")
    gps_time_str = fields.Char("GPS Time")
    gps_time = fields.Datetime("GPS Time")
    mileage = fields.Integer("Mileage")
    temperature = fields.Float("temperature")
    position = fields.Text("Position")
    oil_num = fields.Float("Oil")
    log_line = fields.One2many("fleet.vehicle.log", "vehicle_id", "Logs")
    oot = fields.Boolean("Out of Fences", readonly=True)
    loaded = fields.Boolean("Loaded", readonly=True)

    @api.depends('latitude', 'longitude')
    def _compute_point(self):
        for rec in self:
            rec.point = geo_fields.GeoPoint.from_latlon(
                self.env.cr, rec.latitude, rec.longitude).wkb_hex

    def set_lang_long(self):
        point_x, point_y = geojson.loads(self.point)['coordinates']
        inproj = Proj(init='epsg:3857')
        outproj = Proj(init='epsg:4326')
        longitude, latitude = transform(inproj, outproj, point_x, point_y)
        self.latitude = latitude
        self.longitude = longitude

    # @api.onchange('point')
    # def onchange_geo_point(self):
    #     if self.point:
    #         self.set_lang_long()

    @api.depends('point')
    def _set_lat_long(self):
        for rec in self:
            if rec.point:
                rec.set_lang_long()


    @api.one
    def get_point_position_from_traccar(self) :
        traccar_url_config = self.env['ir.config_parameter'].sudo().search([('key','=','traccar_url')])
        url = traccar_url_config.value

        if url :
            url = url + "/api/positions"
            headers = {
                'Authorization': 'Basic YWRtaW46YWRtaW4=',
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
            r = requests.get(url, headers=headers)
            if r.status_code == requests.codes.ok :
                response = r.json()

                for r in response :
                    if self.traccar_device_id == r['deviceId'] :
                        self.write({
                            'latitude' : r['latitude'],
                            'longitude' : r['longitude'],
                        })
    
    @api.multi
    def get_coord_json(self):
        webtrack_url_config = self.env['ir.config_parameter'].sudo().search([('key','=','webtrack_url')])
        url = webtrack_url_config.value
        if not url :
            raise ValidationError("Please set webtrack URL in system parameter")
        res_loads = []
        for url_str in url.split('\n'):
            response = urllib2.urlopen(url_str, None, 10.0)
            json_text = response.read()
            res_loads += json.loads(json_text)
        return res_loads

    @api.multi
    def act_compute_coord(self):
        if self.license_plate:
            res_loads = self.get_coord_json()
            for load in res_loads:
                if self.license_plate == load['VName']:
                    GPSTime = load['GPSTime'].replace('T',' ').replace('Z','')
                    gps_time = fields.Datetime.from_string(GPSTime).replace(tzinfo=tz.gettz('Asia/Jakarta'))
                    gps_time = gps_time.astimezone(tz.gettz('UTC'))
                    GPSTime = fields.Datetime.to_string(gps_time)

                    vals = {
                         'gps_time_str':load['GPSTime'].replace('T',' ').replace('Z',''),
                         'gps_time':GPSTime,
                         'odometer':load['Mileage']/1000.0,
                         'longitude':load['lon'],
                         'latitude':load['lat'],
                         'high':load['High'],
                         'position':load['Position'],
                         'speed':load['Speed'],
                         'oil_num':load['oilnum'],
                         'sim':load['sim'],
                         'temperature':load['Temperature'],
                        }
                    self.write(vals)
                    vals['vehicle_id'] = self.id
                    log = self.env['fleet.vehicle.log'].create(vals)
        elif not self.license_plate :
            raise ValidationError("License plate is not correctly set")

    @api.multi
    def check_distance(self, lat1, lon1, lat2, lon2):
        R = 6378.137; # Radius of earth in KM
        dLat = lat2 * math.pi / 180 - lat1 * math.pi / 180;
        dLon = lon2 * math.pi / 180 - lon1 * math.pi / 180;
        a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(lat1 * math.pi / 180) * math.cos(lat2 * math.pi / 180) * math.sin(dLon/2) * math.sin(dLon/2);
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a));
        d = R * c;
        return d * 1000; # meters

    @api.model
    def check_on_track(self):
        self.env.cr.execute("""
            SELECT vehicle.id, vehicle.license_plate, vehicle.latitude as cur_lat, vehicle.longitude as cur_long, route_log.latitude as target_lat, route_log.longitude as target_long,
            SQRT(POW((route_log.latitude-vehicle.latitude),2) + POW((route_log.longitude-vehicle.longitude),2)) as distance

            FROM fleet_vehicle as vehicle
            LEFT JOIN tms_travel as travel ON (travel.unit_id = vehicle.id)
            LEFT JOIN tms_route as route ON (route.id = travel.route_id)
            LEFT JOIN tms_route_log as route_log ON (route.id = route_log.route_id)""")

        vehicles = {}
        for rec in self.env.cr.fetchall():
            vals = {
                'id':rec[0],
                'name':rec[1],
                'cur_lat':rec[2],
                'cur_long':rec[3],
                'target_lat':rec[4],
                'target_long':rec[5],
                'distance':rec[6]
            }
            if vals['name'] and vals['name'] not in vehicles or (vals['distance'] and vals['distance'] < vehicles[vals['name']]['distance']) :
                vehicles.update({str(vals['name']) : vals})

        vehicle_ids = []
        vehicle_names = []
        for key, vhc in vehicles.iteritems(): 
            if vhc['cur_lat'] and vhc['cur_long'] and vhc['target_lat'] and vhc['target_long'] :
                distance = self.check_distance(vhc['target_lat'], vhc['target_long'], vhc['cur_lat'], vhc['cur_long'])
                if distance > 1000 :
                    print("Vehice " +vhc['name']+ " is out of track")
                    vehicle_ids.append(vhc['id'])
                    vehicle_names.append(vhc['name'])

        try :
            self.send_notification(vehicle_ids, vehicle_names, 'Kendaraan keluar dari rute yang ditentukan')
        except :
            print('Send Notification Error')

        self.browse(vehicle_ids).write({'oot' : True})

    @api.model
    def sync_position(self):
        res_loads = self.get_coord_json()
        vehicle_log = self.env['fleet.vehicle.log']
        for load in res_loads:
            vehicle = self.search([('license_plate','=',load['VName'])], limit=1)
            if vehicle.id:
                GPSTime = load['GPSTime'].replace('T',' ').replace('Z','')
                gps_time = fields.Datetime.from_string(GPSTime).replace(tzinfo=tz.gettz('Asia/Jakarta'))
                gps_time = gps_time.astimezone(tz.gettz('UTC'))
                GPSTime = fields.Datetime.to_string(gps_time)

                last_log = vehicle_log.search([('vehicle_id', '=', vehicle.id)], limit=1, order="id desc")
                if last_log :
                    last_log = last_log[0]
                    if last_log.gps_time_str == load['GPSTime'].replace('T',' ').replace('Z','') :
                        continue
                
                vals = {
                        'gps_time_str':load['GPSTime'].replace('T',' ').replace('Z',''),
                        'gps_time':GPSTime,
                        'odometer':load['Mileage']/1000.0,
                        'longitude':load['lon'],
                        'latitude':load['lat'],
                        'high':load['High'],
                        'position':load['Position'],
                        'speed':load['Speed'],
                        'oil_num':load['oilnum'],
                        'sim':load['sim'],
                        'temperature':load['Temperature'],
                        }
                vehicle.write(vals)      
                vals.update(vehicle_id=vehicle.id)
                vals.update(mileage = vals.pop('odometer'))
                log_id = vehicle_log.create(vals)
                print("Vehicle Log Created " + vehicle.license_plate + " " + str(vehicle.id))

                travel = self.env['tms.travel'].search([('unit_id','=',vehicle.id)], limit=1)
                cur_lat = load['lat']
                cur_lon = load['lon']
                if travel.id :
                    # mengecek posisi apakah didalam radius loading
                    loading_lat = travel.departure_id.latitude
                    loading_lon = travel.departure_id.longitude
                    unloading_lat = travel.arrival_id.latitude
                    unloading_lon = travel.arrival_id.longitude
                    if not travel.loading_id and travel.check_distance(cur_lat, cur_lon, loading_lat, loading_lon) < 500.0:
                        travel.state = 'loading'
                        travel.loading_id = log_id.id
                        vehicle.loaded = True

                    # keluar loading
                    elif not travel.loading_out_id and travel.state == 'loading' and travel.check_distance(cur_lat, cur_lon, loading_lat, loading_lon) > 500.0:
                        travel.state = 'progress'
                        travel.loading_out_id = log_id.id
                        vehicle.loaded = True

                    # mengecek posisi apakah didalam radius unloading 
                    elif not travel.unloading_id and travel.state == 'progress' and travel.check_distance(cur_lat, cur_lon, unloading_lat, unloading_lon) < 500.0:
                        travel.state = 'unloading'
                        travel.unloading_id = log_id.id
                        vehicle.loaded = False

                    # mengecek posisi apakah didalam radius unloading 
                    elif not travel.unloading_out_id and travel.state == 'unloading' and travel.check_distance(cur_lat, cur_lon, unloading_lat, unloading_lon) > 500.0:
                        travel.state = 'done'
                        travel.unloading_out_id = log_id.id
                        vehicle.loaded = False

    @api.model
    def check_state(self):
        res_loads = self.get_coord_json()
        vehicle_log = self.env['fleet.vehicle.log']
        for load in res_loads:
            vehicle = self.search([('license_plate','=',load['VName'])], limit=1)
            if vehicle.id:
                travel = self.env['tms.travel'].search([('unit_id','=',vehicle.id)], limit=1)
                cur_lat = load['lat']
                cur_lon = load['lon']

                last_log = vehicle_log.search([('vehicle_id', '=', vehicle.id)], limit=1, order="id desc")
                if last_log :
                    log_id = last_log[0]

                if travel.id and log_id:
                    # mengecek posisi apakah didalam radius loading
                    loading_lat = travel.departure_id.latitude
                    loading_lon = travel.departure_id.longitude
                    unloading_lat = travel.arrival_id.latitude
                    unloading_lon = travel.arrival_id.longitude
                    if travel.check_distance(cur_lat, cur_lon, loading_lat, loading_lon) < 500.0:
                        travel.state = 'loading'
                        travel.loading_id = log_id.id
                        vehicle.loaded = True

                    # keluar loading
                    elif travel.state == 'loading' and travel.check_distance(cur_lat, cur_lon, loading_lat, loading_lon) > 500.0:
                        travel.state = 'progress'
                        travel.loading_out_id = log_id.id
                        vehicle.loaded = True

                    # mengecek posisi apakah didalam radius unloading 
                    elif travel.state == 'progress' and travel.check_distance(cur_lat, cur_lon, unloading_lat, unloading_lon) < 500.0:
                        travel.state = 'unloading'
                        travel.unloading_id = log_id.id
                        vehicle.loaded = False

                    # mengecek posisi apakah didalam radius unloading 
                    elif travel.state == 'unloading' and travel.check_distance(cur_lat, cur_lon, unloading_lat, unloading_lon) > 500.0:
                        travel.state = 'done'
                        travel.unloading_out_id = log_id.id
                        vehicle.loaded = False
                

    @api.model
    def send_notification(self, vehicle_ids, vehicle_names, message_body):
        for index, vhc_id in enumerate(vehicle_ids):

            _logger.debug('Vehicle {} Out of track'.format(vehicle_names[index]))

            message_body = '[' + vehicle_names[index] + ']' + ' ' + message_body

            # Admin
            user_admin = request.env['ir.model.data'].sudo().get_object('base', 'user_root')
            if not user_admin :
                raise ValidationError("User Admin not found")
            partner_admin = user_admin.partner_id
            email = partner_admin.email
            mobile_number = partner_admin.email

            # Create Notif Object
            Notification = self.env['fleet.vehicle.notification'].sudo()
            notification = Notification.create({
                'vehicle_id' : vehicle_ids[index],
                'message_body' : message_body,
                'email_state' : 'wait',
                'mobile_state' : 'wait',
            })

            # SMS with RajaSMS
            mobile_number = mobile_number or self.env['ir.config_parameter'].sudo().search([('key','=','mobile_number')]).value
            if mobile_number :
                url = 'http://45.32.118.255/sms/api_sms_reguler_send_json.php' #'http://45.76.156.114' http://45.32.107.195/
                api_key = '0d57b57820549f4bf660436b099fddec'
                payload = {
                    "apikey": api_key,
                    "callbackurl": "http://google.com",
                    "datapacket": [
                        {
                            "number": mobile_number,
                            "message": message_body,
                            "sendingdatetime": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        }
                    ]
                }
                try:
                    r = requests.post(url, json=payload)
                    result = r.json()
                    notification.mobile_state = 'sent'
                except:
                    raise ValidationError("Cannot send sms")

            # Email
            email = email or self.env['ir.config_parameter'].sudo().search([('key','=','email')]).value
            if email :
                Mail = self.env['mail.mail']
                vals = {
                    # 'email_from' : 'TMS Notification Center <'+email+'>',
                    'email_to' : email,
                    'subject' : 'TMS Notification',
                    'body' : message_body,
                    'body_html' : '<div>'+message_body+'</div>'
                }
                mail = Mail.create(vals)
                mail.send()
                notification.email_state = 'sent'
        
class FleetVehicleLog(models.Model):
    _name = "fleet.vehicle.log"
    _description = "Vehicle Logging"
    _rec_name = "gps_time"
    _order = "id desc"
    
    active = fields.Boolean(default=True)
    sim = fields.Text("SIM")
    longitude = fields.Float("Longitude", digits=(20,10))
    latitude = fields.Float("Latitude", digits=(20,10))
    high = fields.Float("High")
    speed = fields.Float("Speed")
    gps_time_str = fields.Char("GPS Time")
    gps_time = fields.Datetime("GPS Time")
    mileage = fields.Integer("Mileage")
    temperature = fields.Float("temperature")
    position = fields.Text("Position")
    oil_num = fields.Float("Oil")
    vehicle_id = fields.Many2one("fleet.vehicle", "Vehicle")

class GeoVectorLayer(models.Model):
    _inherit = 'geoengine.vector.layer'

    traccar_url = fields.Char('Traccar URL')

class VehicleNotification(models.Model):
    _name = 'fleet.vehicle.notification'

    vehicle_id = fields.Many2one("fleet.vehicle", "Vehicle")
    message_body = fields.Text("Message Body")
    mobile_state = fields.Selection([
        ('wait', 'Wait'),
        ('sent', 'Sent')
    ])
    email_state = fields.Selection([
        ('wait', 'Wait'),
        ('sent', 'Sent')
    ])
