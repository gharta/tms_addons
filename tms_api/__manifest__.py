# -*- coding: utf-8 -*-
{
    'name': "TMS API",

    'summary': """
        TMS gps routing, gps log, geo fencing""",

    'description': """
        TMS gps log in vehicle and routing the travel route by gps log, geo fencing
    """,

    'author': "Arkana",
    'website': "https://www.arkana.co.id",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base','tms','operating_unit','base_geoengine'
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}